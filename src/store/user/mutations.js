let exp = {};

exp.SET_CURRENT_USER  = (state, payload) => {
  //todo: validation
  state.currentUser = payload;
  state.isLoggedIn = !!payload;
};

export default exp;
