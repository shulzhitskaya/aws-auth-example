import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";

const state = {
  currentUser: null,
  isLoggedIn: false
};

export default {
  strict: true,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
