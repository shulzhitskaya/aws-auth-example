import { Auth } from 'aws-amplify';

const GET_AUTH_USER = () => { 
    return Auth.currentUserInfo();
};

const SIGN_UP = () => {

}

const SIGN_OUT = () =>{
    return Auth.signOut();
}

export default{
    GET_AUTH_USER,
    SIGN_UP,
    SIGN_OUT
};