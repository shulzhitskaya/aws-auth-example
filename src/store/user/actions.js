import service from "./aws-logic";
import { mutations } from "./constraints";

let exp = {};

exp.GET_USER = state => {
  return service
    .GET_AUTH_USER()
    .then(responce => {
      const user = responce;
      state.commit(mutations.user.SET_CURRENT_USER, user);
    })
    .catch(err => {
      state.commit(mutations.user.SET_CURRENT_USER, null);
    });
};

exp.SIGN_UP = state => {
  return service.SIGN_UP();
};

exp.SIGN_OUT = state => {
  return service
    .SIGN_OUT()
    .then(response => {
      state.commit(mutations.user.SET_CURRENT_USER, null);
    })
    .catch(err => {});
};

export default exp;
