const currentUser = state => state.currentUser;
const userName = state =>state.currentUser ? state.currentUser.attributes.email: 'N/A';
const isLoggedIn = state => state.isLoggedIn;

export default {
  currentUser,
  isLoggedIn,
  userName
};
