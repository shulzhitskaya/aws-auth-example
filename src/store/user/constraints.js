export const mutations = {
  user: { SET_CURRENT_USER: "SET_CURRENT_USER" }
};

export const actions = {
  user: {
    GET: "user/GET_USER",
    SIGN_OUT: "user/SIGN_OUT",
    SIGN_UP: "user/SIGN_UP"
  }
};

export const getters = {
  user: { IS_LOGGED_IN: "user/isLoggedIn", USER_NAME: "user/userName" }
};
