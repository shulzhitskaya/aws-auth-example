import Vue from "vue";
import Vuex from "vuex";

import User from "./user/index";

Vue.use(Vuex);

const modules = {
  user: User
};

export default new Vuex.Store({
  modules
});
