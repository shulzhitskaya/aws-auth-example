import Vue from "vue";
import App from "./App.vue";

import store from "./store/index";

import VueRouter from 'vue-router';
import router from './routes';

import Amplify, * as AmplifyModules from "aws-amplify";
import { AmplifyPlugin } from "aws-amplify-vue";

Vue.config.productionTip = false;

Amplify.configure({
  Auth: {
    region: "",
    userPoolId: "",
    userPoolWebClientId: ""
  }
});

Vue.use(AmplifyPlugin, AmplifyModules);
Vue.use(VueRouter);

new Vue({
  render: h => h(App),
  store,
  router
}).$mount("#app");
