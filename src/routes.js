
import VueRouter from "vue-router";

const awsLogin = () => import("./components/AmplifyLogin.vue");
const customLogin = () => import("./components/CustomLogin.vue");

export default new VueRouter({
    mode: "history",
    routes: [
      { path: "/aws-login", component: awsLogin },
      { path: "/custom-login", component: customLogin }
    ]
  });
